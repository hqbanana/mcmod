package com.hqbanana.endgamestuffmod.util.handlers;

import com.hqbanana.endgamestuffmod.tileentities.TileEntityRubyFurnace;

import net.minecraftforge.fml.common.registry.GameRegistry;

public class TileEntityHandler {
	public static void registerTileEntities() {
		GameRegistry.registerTileEntity(TileEntityRubyFurnace.class, "ruby_furnace");
	}
}
