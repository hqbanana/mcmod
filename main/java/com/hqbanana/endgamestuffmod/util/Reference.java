package com.hqbanana.endgamestuffmod.util;

public class Reference {
	public static final String MOD_ID = "egsm";
	public static final String NAME = "Endgame stuff mod";
	public static final String VERSION = "1.0";
	public static final String ACCEPTED_VERSIONS = "[1.12.2]";
	public static final String CLIENT_PROXY_CLASS = "com.hqbanana.endgamestuffmod.proxy.ClientProxy";
	public static final String COMMON_PROXY_CLASS = "com.hqbanana.endgamestuffmod.proxy.CommonProxy";
	
	public static final int GUI_RUBY_FURNACE = 0;
}
